import { Module } from '@nestjs/common';
import { EmpresaService } from './empresa.service';
import { EmpresaController } from './empresa.controller';
import { ConectionService } from '../core/database';

@Module({
  providers: [EmpresaService, ConectionService],
  controllers: [EmpresaController]
})
export class EmpresaModule {}
