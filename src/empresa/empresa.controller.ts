import { Controller, Get, Post, Response, Body, HttpStatus } from '@nestjs/common';
import { EmpresaService } from './empresa.service';

@Controller('api/empresa')
export class EmpresaController {
    constructor(private empresaService: EmpresaService){}

    @Get('/listar')
    public async listarEmpresa(@Response() res): Promise<any>{
        const result = await this.empresaService.listarEmpresa();
        return res.status(HttpStatus.OK).json(result)
    }

}
