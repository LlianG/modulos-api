import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { ConectionService } from '../core/database';
import  * as sql from 'mssql';

@Injectable()
export class EmpresaService {
    constructor(private conectionService: ConectionService){}

    async listarEmpresa(): Promise<any>{
        const result =  await this.conectionService.getConnection().then(pool => {
                const request = new sql.Request(pool);
                    return request.query('select * from gen_empresa').then(result => {
                        const res = {
                            mensaje: "Procedimiento ejecutado correctamente",
                            filas: result,
                            datos: result.returnValue
                        }
                        return res;
                    }).catch(err => {
                        console.log(err)
                        throw new HttpException({
                            status: HttpStatus.FORBIDDEN,
                            error: 'No se pudo ejecutar el procedimiento.',
                        }, 500);
                })
                }).catch(err => {
                    console.log(err)
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No se pudo ejecutar el procedimiento.',
                    }, 500);
                })
        return result;
    }
}
