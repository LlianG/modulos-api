import { Module } from '@nestjs/common';
import { FacturacionMobilsoftController } from './facturacion-mobilsoft.controller';
import { FacturacionMobilsoftService } from './facturacion-mobilsoft.service';
import { ConectionService } from '../core/database';

@Module({
  controllers: [FacturacionMobilsoftController],
  providers: [FacturacionMobilsoftService, ConectionService],
  exports: [FacturacionMobilsoftService]
})
export class FacturacionMobilsoftModule {}