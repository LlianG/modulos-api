import { Test, TestingModule } from '@nestjs/testing';
import { FacturacionMobilsoftController } from './facturacion-mobilsoft.controller';

describe('FacturacionMobilsoft Controller', () => {
  let controller: FacturacionMobilsoftController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FacturacionMobilsoftController],
    }).compile();

    controller = module.get<FacturacionMobilsoftController>(FacturacionMobilsoftController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
