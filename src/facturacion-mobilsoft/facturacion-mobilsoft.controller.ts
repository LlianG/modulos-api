import { Controller, Body, Post, Get, Patch, Param, Response, HttpStatus } from '@nestjs/common';
import { FacturacionMobilsoftService } from './facturacion-mobilsoft.service';

@Controller('/api/facturacion-mobilsoft')
export class FacturacionMobilsoftController {
    constructor(private facturacionMobilsoftService: FacturacionMobilsoftService){}


    @Get('/listar')
    public async listarFacturas(@Response() res) {
        const result = await this.facturacionMobilsoftService.listarFacturas();
        return res.status(HttpStatus.OK).json(result);
    }
    @Get('/listar_formas_pago')
    public async listarFormasPago(@Response() res){
        const result = await this.facturacionMobilsoftService.listarFormasPago();
        return res.status(HttpStatus.OK).json(result);
    }

    @Post('/guardar')
    public async guardarFactura(@Response() res, @Body() data: any){
        const result = await this.facturacionMobilsoftService.guardarFactura(data);
        return res.status(HttpStatus.OK).json(result);
    }
    @Post('/listar_factura')
    public async listarFactura(@Response() res, @Body() data: any){
        const result = await this.facturacionMobilsoftService.listarFactura(data);
        return res.status(HttpStatus.OK).json(result);
    }
    @Post('/consecutivo_factura')
    public async consecutivoFactura(@Response() res, @Body() data: any){
        const result = await this.facturacionMobilsoftService.consecutivoFactura(data);
        return res.status(HttpStatus.OK).json(result);
    }
    @Post('/cupo_utilizado')
    public async cupoUtilizado(@Response() res, @Body() data: any){
        const result = await this.facturacionMobilsoftService.cupoUtilizado(data);
        return res.status(HttpStatus.OK).json(result);
    }

}
