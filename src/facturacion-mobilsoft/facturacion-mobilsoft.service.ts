import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { ConectionService } from '../core/database';
import * as sql from 'mssql';
@Injectable()
export class FacturacionMobilsoftService {
    constructor(private conectionService: ConectionService){

    }
    async listarFacturas(): Promise<any> {
        return this.conectionService.getConnection().then(pool => {
            var request = new sql.Request(pool);
            request.input('accion', sql.Char(1), 'T')
            return request.execute('sp_factura').then(result => {
                return {
                    mensaje: "Procedimiento ejecutado correctamente",
                    filas: result,
                    datos: result.returnValue
                }
            }).catch(err => {
                console.log(err)
                throw new HttpException({
                    status: HttpStatus.FORBIDDEN,
                    error: 'No se pudo ejecutar el procedimiento.',
                }, 500);
            })
        }).catch(err => {
            console.log(err);
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No se pudo ejecutar el procedimiento.',
            }, 500);
        })
    }
    async listarFormasPago(): Promise<any> {
        return this.conectionService.getConnection().then(pool => {
            var request = new sql.Request(pool);
            return request.query('select * from [dbo].[Tes_formapago] where estado = 1').then(result => {
                return {
                    mensaje: "Procedimiento ejecutado correctamente",
                    filas: result,
                    datos: result.returnValue
                }
            }).catch(err => {
                console.log(err)
                throw new HttpException({
                    status: HttpStatus.FORBIDDEN,
                    error: 'No se pudo ejecutar el procedimiento.',
                }, 500);
            })
        }).catch(err => {
            console.log(err);
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No se pudo ejecutar el procedimiento.',
            }, 500);
        })
    }
    async listarFactura(data: any): Promise<any>{
        return this.conectionService.getConnection().then(pool => {
            var request = new sql.Request(pool);
            request.input('id_factura', sql.Int, data.id_factura)
            request.input('accion', sql.Char(1), 'L')
            return request.execute('sp_factura').then(result => {
                return {
                    mensaje: "Procedimiento ejecutado correctamente",
                    filas: result,
                    datos: result.returnValue
                }
            }).catch(err => {
                console.log(err)
                throw new HttpException({
                    status: HttpStatus.FORBIDDEN,
                    error: 'No se pudo ejecutar el procedimiento.',
                }, 500);
            })
        }).catch(err => {
            console.log(err);
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No se pudo ejecutar el procedimiento.',
            }, 500);
        })
    }
    async cupoUtilizado(data: any): Promise<any>{
        return this.conectionService.getConnection().then(pool => {
            var request = new sql.Request(pool);
            request.input('codigo_tercero', sql.VarChar(20), data.IDENTIFICACION_CLIENTE)
            request.input('accion', sql.Char(1), 'M')
            return request.execute('sp_factura').then(result => {
                return {
                    mensaje: "Procedimiento ejecutado correctamente",
                    filas: result,
                    datos: result.returnValue
                }
            }).catch(err => {
                console.log(err)
                throw new HttpException({
                    status: HttpStatus.FORBIDDEN,
                    error: 'No se pudo ejecutar el procedimiento.',
                }, 500);
            })
        }).catch(err => {
            console.log(err);
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No se pudo ejecutar el procedimiento.',
            }, 500);
        })
    }
    async consecutivoFactura(data: any): Promise<any>{
        return this.conectionService.getConnection().then(pool => {
            var request = new sql.Request(pool);
            request.input('codalm', sql.VarChar(3), data.almacen)
            request.input('tipodco', sql.Char(2), 'FV')
            request.input('clase', sql.Char(1), 'U')
            request.input('detalle', sql.VarChar(100), 'Factura de venta a clientes')
            return request.execute('sp_consecutivo').then(result => {
                return {
                    mensaje: "Procedimiento ejecutado correctamente",
                    filas: result,
                    datos: result.returnValue
                }
            }).catch(err => {
                console.log(err)
                throw new HttpException({
                    status: HttpStatus.FORBIDDEN,
                    error: 'No se pudo ejecutar el procedimiento.',
                }, 500);
            })
        }).catch(err => {
            console.log(err);
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No se pudo ejecutar el procedimiento.',
            }, 500);
        })
    }

    async guardarFactura(req: any): Promise<any>{
        const result = await this.conectionService.getConnection().then(pool => {
            const request = new sql.Request(pool);

            const detalle = new sql.Table();
            detalle.columns.add('id_producto', sql.Int)
            detalle.columns.add('id_medida', sql.Int)
            detalle.columns.add('tasa_iva', sql.Numeric(5,3))
            detalle.columns.add('tasa_descuento', sql.Numeric(5,3))
            detalle.columns.add('cantidad_pedida', sql.Numeric(18,2))
            detalle.columns.add('cantidad_facturada', sql.Numeric(18,2))
            detalle.columns.add('costo_operacion', sql.Numeric(18,2))
            detalle.columns.add('precio_operacion', sql.Numeric(18,2))
            detalle.columns.add('cantidad_medida', sql.Int)
            for (let i = 0; i < req.servicios.length; i++) {
                const element = req.servicios[i];
                detalle.rows.add(
                    element.SERVICIO,
                    element.UNIDAD,
                    element.IVA,
                    element.desund,
                    1,
                    1,
                    element.SUBTOTAL,
                    element.SUBTOTAL,
                    element.CANTIDAD_UNIDAD
                )
            }

            request.input('codigo_tercero', sql.VarChar(20), req.formData.IDENTIFICACION_CLIENTE);
            request.input('codigo_cuenta', sql.VarChar(15), req.formData.CUENTA_BANCARIA);
            request.input('id_vendedor', sql.Int, 1);
            request.input('id_bodega', sql.Int, 1);
            request.input('id_usuario', sql.Int, 1);
            request.input('tipo_operacion', sql.Char(2), 'FV');
            request.input('consecutivo', sql.VarChar(15), req.formData.CONSECUTIVO);
            request.input('comprobante', sql.VarChar(15), req.formData.CODIGO_ALMACEN+'-'+req.formData.CONSECUTIVO);
            request.input('subtotal', sql.Numeric(18,2), req.facturacion.SUBTOTAL);
            request.input('total', sql.Numeric(18,2), req.facturacion.TOTAL);
            request.input('valor_fletes', sql.Numeric(18,2), req.facturacion.FLETES);
            request.input('valor_descuento', sql.Numeric(18,2), 0);
            request.input('valor_retencion_fuente', sql.Numeric(18,2), 0);
            request.input('valor_retencion_ica', sql.Numeric(18,2), 0);
            request.input('valor_retencion_iva', sql.Numeric(18,2), 0);
            request.input('valor_abono', sql.Numeric(18,2), 0);
            request.input('valor_iva', sql.Numeric(18,2), req.facturacion.IVA);
            request.input('observaciones', sql.Text,  req.formData.COMENTARIO);
            request.input('modo_pago', sql.Char(2),  req.formData.MODALIDAD_PAGO);
            request.input('codalm', sql.Char(3),  req.formData.CODIGO_ALMACEN)
            request.input('fecha_vencimiento', sql.Date, new Date(req.formData.FECHA_VENCIMIENTO).getFullYear()+'-'+[new Date(req.formData.FECHA_VENCIMIENTO).getMonth()+1]+'-'+new Date(req.formData.FECHA_VENCIMIENTO).getDate())
            request.input('detalle_factura', sql.TVP, detalle)
            request.input('accion', sql.Char(1), 'D');
            return request.execute('sp_factura').then(result => {
                return {
                    mensaje: "Procedimiento ejecutado correctamente",
                    filas: result,
                    datos: result.returnValue,
                    status: true
                }
            }).catch(err => {
                console.log(err)
                throw new HttpException({
                    status: HttpStatus.FORBIDDEN,
                    error: 'Error al guardar encabezado',
                }, 500);
            })
        }).catch(err => {
            console.log(err)
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No se pudo ejecutar el procedimiento.',
            }, 500);
        })
        return result;
    }
}
