import { Test, TestingModule } from '@nestjs/testing';
import { FacturacionMobilsoftService } from './facturacion-mobilsoft.service';

describe('FacturacionMobilsoftService', () => {
  let service: FacturacionMobilsoftService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FacturacionMobilsoftService],
    }).compile();

    service = module.get<FacturacionMobilsoftService>(FacturacionMobilsoftService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
