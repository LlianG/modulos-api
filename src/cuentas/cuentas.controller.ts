import { Controller, Get, Post, Response, Body, HttpStatus} from '@nestjs/common';
import { CuentasService } from './cuentas.service';

@Controller('api/cuentas')
export class CuentasController {
    constructor(private cuentasService: CuentasService){

    }

    @Post('/listar')
    public async listarCuentas(@Response() res, @Body() data){
        const result = await this.cuentasService.listarCuentas(data);
        return res.status(HttpStatus.OK).json(result);
    }
}
