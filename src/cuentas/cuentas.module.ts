import { Module } from '@nestjs/common';
import { CuentasService } from './cuentas.service';
import { CuentasController } from './cuentas.controller';
import { ConectionService } from '../core/database';

@Module({
  providers: [CuentasService, ConectionService],
  controllers: [CuentasController]
})
export class CuentasModule {}
