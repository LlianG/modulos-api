import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FacturacionMobilsoftModule } from './facturacion-mobilsoft/facturacion-mobilsoft.module';
import { ConectionService } from './core/database';
import { TercerosModule } from './terceros/terceros.module';
import { CiudadModule } from './ciudad/ciudad.module';
import { DepartamentoModule } from './departamento/departamento.module';
import { AlmacenModule } from './almacen/almacen.module';
import { CuentasModule } from './cuentas/cuentas.module';
import { ServiciosModule } from './servicios/servicios.module';
import { EmpresaModule } from './empresa/empresa.module';
import { VendedoresModule } from './vendedores/vendedores.module';
import { ReciboCajaModule } from './recibo-caja/recibo-caja.module';
import { ConceptoModule } from './concepto/concepto.module';
@Module({
  imports: [FacturacionMobilsoftModule, TercerosModule, CiudadModule, DepartamentoModule, AlmacenModule, CuentasModule, ServiciosModule, EmpresaModule, VendedoresModule, ReciboCajaModule, ConceptoModule],
  exports: [FacturacionMobilsoftModule],
  controllers: [AppController],
  providers: [AppService, ConectionService],
})
export class AppModule {}
