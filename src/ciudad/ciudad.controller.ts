import { Controller, Get, Post, Body, Response, HttpStatus } from '@nestjs/common';
import { CiudadService } from './ciudad.service';

@Controller('api/ciudades')
export class CiudadController {
    constructor(private ciudadService: CiudadService){}
    @Get('/listar')
    public async listarCiudades(@Response() res) {
        const result = await this.ciudadService.listarCiudades();
        return res.status(HttpStatus.OK).json(result);
    }
}
