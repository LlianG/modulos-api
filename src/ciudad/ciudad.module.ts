import { Module } from '@nestjs/common';
import { CiudadService } from './ciudad.service';
import { CiudadController } from './ciudad.controller';
import { ConectionService } from '../core/database';

@Module({
  providers: [CiudadService, ConectionService],
  controllers: [CiudadController]
})
export class CiudadModule {}
