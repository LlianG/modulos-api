import { Injectable } from '@nestjs/common';
import { ConectionService } from '../core/database';
import * as sql from 'mssql';

@Injectable()
export class CiudadService {
    constructor(private conectionService: ConectionService){

    }
    public async listarCiudades(): Promise <any>{
        const result =  await this.conectionService.getConnection().then(pool => {
                            const request = new sql.Request(pool);
                            return request.query('select * from gen_municipios').then(result => {
                                const res = {
                                    mensaje: "Procedimiento ejecutado correctamente",
                                    filas: result,
                                    datos: result.returnValue
                                }
                                return res;
                            }).catch(err => {
                                return {
                                    mensaje: "No se pudo ejecutar el procedimiento",
                                    error: err
                                }
                        })
                        }).catch(err => {
                            console.log(err)
                        })
        return result;
    }
}
