import { Module } from '@nestjs/common';
import { TercerosService } from './terceros.service';
import { TercerosController } from './terceros.controller';
import { ConectionService } from '../core/database';

@Module({
  providers: [TercerosService, ConectionService],
  controllers: [TercerosController]
})
export class TercerosModule {}
