import { Controller, Get, Post, Body, Response, HttpStatus} from '@nestjs/common';
import { TercerosService } from './terceros.service';

@Controller('api/terceros')
export class TercerosController {

    constructor(private tercerosService: TercerosService){

    }
    @Get('/listar')
    public async listarTerceros(@Response() res) {
        const result = await this.tercerosService.listarTerceros();
        return res.status(HttpStatus.OK).json(result);
    }
}
