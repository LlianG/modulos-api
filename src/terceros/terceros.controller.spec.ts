import { Test, TestingModule } from '@nestjs/testing';
import { TercerosController } from './terceros.controller';

describe('Terceros Controller', () => {
  let controller: TercerosController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TercerosController],
    }).compile();

    controller = module.get<TercerosController>(TercerosController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
