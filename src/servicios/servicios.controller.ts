import { Controller, Get, Post, Response, HttpStatus } from '@nestjs/common';
import { ServiciosService } from './servicios.service';

@Controller('api/servicios')
export class ServiciosController {
    constructor(private serviciosService: ServiciosService){ 

    }

    @Get('/listar')
    public async listarServicios(@Response() res){ 
        const result = await this.serviciosService.listarServicios();
        return res.status(HttpStatus.OK).json(result);
    }
}
