import { Module } from '@nestjs/common';
import { ServiciosService } from './servicios.service';
import { ServiciosController } from './servicios.controller';
import { ConectionService } from '../core/database'

@Module({
  providers: [ServiciosService,ConectionService],
  controllers: [ServiciosController]
})
export class ServiciosModule {}
