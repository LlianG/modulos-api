import { Injectable } from '@nestjs/common';
import { ConectionService } from '../core/database';
import * as sql from 'mssql';

@Injectable()
export class DepartamentoService {
    constructor(private conectionService: ConectionService){

    }
    public async listarDepartamentos(): Promise <any>{
        const result =  await this.conectionService.getConnection().then(pool => {
                            const request = new sql.Request(pool);
                            return request.query('select * from gen_departamentos').then(result => {
                                const res = {
                                    mensaje: "Procedimiento ejecutado correctamente",
                                    filas: result,
                                    datos: result.returnValue
                                }
                                return res;
                            }).catch(err => {
                                return {
                                    mensaje: "No se pudo ejecutar el procedimiento",
                                    error: err
                                }
                        })
                        }).catch(err => {
                            console.log(err)
                        })
        return result;
    }
}
