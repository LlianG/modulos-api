import { Controller, Get, Post, Body, Response, HttpStatus } from '@nestjs/common';
import { DepartamentoService } from './departamento.service';

@Controller('api/departamentos')
export class DepartamentoController {
    constructor(private departamentoService: DepartamentoService){

    }
    @Get('/listar')
    public async listarDepartamentos(@Response() res) {
        const result = await this.departamentoService.listarDepartamentos();
        return res.status(HttpStatus.OK).json(result);
    }
}
