import { Module } from '@nestjs/common';
import { DepartamentoService } from './departamento.service';
import { DepartamentoController } from './departamento.controller';
import { ConectionService } from '../core/database'; 

@Module({
  providers: [DepartamentoService, ConectionService],
  controllers: [DepartamentoController]
})
export class DepartamentoModule {}
