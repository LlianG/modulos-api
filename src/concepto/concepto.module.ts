import { Module } from '@nestjs/common';
import { ConceptoService } from './concepto.service';
import { ConceptoController } from './concepto.controller';
import { ConectionService } from '../core/database';
@Module({
  providers: [ConceptoService, ConectionService],
  controllers: [ConceptoController]
})
export class ConceptoModule {}
