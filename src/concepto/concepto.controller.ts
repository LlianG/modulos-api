import { Controller, Get, Post, Response, Body, HttpStatus } from '@nestjs/common';
import { ConceptoService } from './concepto.service';

@Controller('api/concepto')
export class ConceptoController {
    constructor(private conceptoService: ConceptoService){}
    @Get('/listar')
    public async listarConceptos(@Response() res): Promise<any>{
        const result = await this.conceptoService.listarConceptos();
        return res.status(HttpStatus.OK).json(result)
    }

}
