import { Injectable, HttpException,  HttpStatus} from '@nestjs/common';
import { ConectionService } from '../core/database';
import * as sql from 'mssql'

@Injectable()
export class ConceptoService {
    constructor(private conectionService: ConectionService){}
    async listarConceptos(){
        return this.conectionService.getConnection().then(pool => {
            var request = new sql.Request(pool);
            return request.query('select c.*, tc.nomtip, tc.codigo from [dbo].[ven_conceptos] c inner join [dbo].[ven_tipo_conceptos] tc on c.tipcon = tc.id').then(result => {
                return {
                    mensaje: "Procedimiento ejecutado correctamente",
                    filas: result,
                    datos: result.returnValue
                }
            }).catch(err => {
                console.log(err)
                throw new HttpException({
                    status: HttpStatus.FORBIDDEN,
                    error: 'No se pudo ejecutar el procedimiento.',
                }, 500);
            })
        }).catch(err => {
            console.log(err);
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No se pudo ejecutar el procedimiento.',
            }, 500);
        })
    }
}
