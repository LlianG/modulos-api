import { Injectable } from '@nestjs/common';
import * as q from 'q';
import * as path from 'path';
import * as sql from 'mssql';
const env = "develop_mobilsoft";
import config from './config.json';

@Injectable()
export class ConectionService {
    getConnection (){
        const deferred = q.defer();
        const pool = new sql.ConnectionPool(config.develop_mobilsoft, errorConnect => {
            if (errorConnect) {
                deferred.reject({
                    menssage: "Error al conectar con la base de datos",
                    description: "Error al establecer conexion con la base de datos",
                    error: errorConnect,
                    config: config
                });
                console.log(errorConnect);
            } else {
                deferred.resolve(pool);
            }
        })
        return deferred.promise;
    }
}


