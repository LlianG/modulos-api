import { Module } from '@nestjs/common';
import { VendedoresService } from './vendedores.service';
import { VendedoresController } from './vendedores.controller';
import { ConectionService } from '../core/database';

@Module({
  providers: [VendedoresService, ConectionService],
  controllers: [VendedoresController]
})
export class VendedoresModule {}
