import { Controller, Get, Post, Body, Response, HttpStatus } from '@nestjs/common';
import { VendedoresService } from './vendedores.service';

@Controller('api/vendedores')
export class VendedoresController {
    constructor(private vendedoresService: VendedoresService){}
    @Get('/listar')
    public async listarVendedores(@Response() res): Promise<any>{
        const result = await this.vendedoresService.listarVendedores();
        return  res.status(HttpStatus.OK).json(result)
    }

}
