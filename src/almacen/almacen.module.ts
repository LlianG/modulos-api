import { Module } from '@nestjs/common';
import { AlmacenService } from './almacen.service';
import { AlmacenController } from './almacen.controller';
import { ConectionService } from '../core/database';

@Module({
  providers: [AlmacenService, ConectionService],
  controllers: [AlmacenController]
})
export class AlmacenModule {
  
}
