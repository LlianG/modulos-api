import { Controller, Get, Post, Body, Response, HttpStatus, Res } from '@nestjs/common';
import { AlmacenService } from './almacen.service';

@Controller('api/almacenes')
export class AlmacenController {
    constructor(private almacenService: AlmacenService){

    }

    @Get('/listar')
    public async listarAlmacenes(@Response() res){
        
        const result = await this.almacenService.listarAlmacenes();
        return res.status(HttpStatus.OK).json(result);
    }
}
