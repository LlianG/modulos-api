import { Test, TestingModule } from '@nestjs/testing';
import { ReciboCajaService } from './recibo-caja.service';

describe('ReciboCajaService', () => {
  let service: ReciboCajaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReciboCajaService],
    }).compile();

    service = module.get<ReciboCajaService>(ReciboCajaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
