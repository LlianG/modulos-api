import { Controller, Get, Post, Response, Body, HttpStatus } from '@nestjs/common';
import { ReciboCajaService } from './recibo-caja.service';

@Controller('api/recibo_caja')
export class ReciboCajaController {
    constructor(private reciboCajaService: ReciboCajaService){}
    
    @Post('/facturas_pendientes')
    public async facturasPendiente(@Response() res, @Body() data): Promise<any>{
        const result = await this.reciboCajaService.facturasPendiente(data);
        return res.status(HttpStatus.OK).json(result)
    }
    @Get('/listar_franquicias')
    public async listarFraquicias(@Response() res): Promise<any>{
        const result = await this.reciboCajaService.listarFraquicias();
        return res.status(HttpStatus.OK).json(result)
    }
}
