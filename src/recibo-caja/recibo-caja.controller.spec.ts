import { Test, TestingModule } from '@nestjs/testing';
import { ReciboCajaController } from './recibo-caja.controller';

describe('ReciboCaja Controller', () => {
  let controller: ReciboCajaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReciboCajaController],
    }).compile();

    controller = module.get<ReciboCajaController>(ReciboCajaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
