import { Module } from '@nestjs/common';
import { ReciboCajaService } from './recibo-caja.service';
import { ReciboCajaController } from './recibo-caja.controller';
import { ConectionService } from '../core/database';

@Module({
  providers: [ReciboCajaService, ConectionService],
  controllers: [ReciboCajaController]
})
export class ReciboCajaModule {}
