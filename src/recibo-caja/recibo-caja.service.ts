import { Injectable, HttpException, HttpStatus} from '@nestjs/common';
import { ConectionService } from '../core/database';
import * as sql from 'mssql'

@Injectable()
export class ReciboCajaService {
    constructor(private conectionService: ConectionService){}
    async facturasPendiente(data: any): Promise<any>{
        return this.conectionService.getConnection().then(pool => {
            var request = new sql.Request(pool);
            
            request.input('codigo_tercero', sql.VarChar(12), data.IDENTIFICACION_CLIENTE)
            request.input('accion', sql.Char(1), 'P')
            return request.execute('sp_recibo_caja').then(result => {
                return {
                    mensaje: "Procedimiento ejecutado correctamente",
                    filas: result,
                    datos: result.returnValue
                }
            }).catch(err => {
                console.log(err)
                throw new HttpException({
                    status: HttpStatus.FORBIDDEN,
                    error: 'No se pudo ejecutar el procedimiento.',
                }, 500);
            })
        }).catch(err => {
            console.log(err);
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No se pudo ejecutar el procedimiento.',
            }, 500);
        })
    }
    async listarFraquicias(): Promise<any>{
        return this.conectionService.getConnection().then(pool => {
            var request = new sql.Request(pool);
            return request.query('select * from tes_franquicia').then(result => {
                return {
                    mensaje: "Procedimiento ejecutado correctamente",
                    filas: result,
                    datos: result.returnValue
                }
            }).catch(err => {
                console.log(err)
                throw new HttpException({
                    status: HttpStatus.FORBIDDEN,
                    error: 'No se pudo ejecutar el procedimiento.',
                }, 500);
            })
        }).catch(err => {
            console.log(err);
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No se pudo ejecutar el procedimiento.',
            }, 500);
        })
    }
}

